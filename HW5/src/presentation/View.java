package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class View {
	private JFrame frame;
	private JPanel panel1;
	private JButton ltask1;
	private JButton ltask2;
	private JButton ltask3;
	private JButton ltask4;
	private Dimension dim ;
	private JLabel tasks;

	public View() {
		frame = new JFrame("Main Frame");
		panel1 = new JPanel();
		ltask1 = new JButton("TASK1");
		ltask2 = new JButton("TASK2");
		ltask3 = new JButton("TASK3");
		ltask4 = new JButton("TASK4");
		tasks = new JLabel("TASKS",SwingConstants.CENTER);
		
		dim= Toolkit.getDefaultToolkit().getScreenSize();
		Color fbBlue = new Color(60, 179, 113);
		Color da = new Color(152, 251, 152);
		Font font = new Font("Verdana", Font.BOLD, 20);
		Font font1 = new Font("Verdana", Font.BOLD, 16);
		
		frame.setBounds(0, 0, 720, 180);
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
		frame.setVisible(true);
		
		panel1.setBounds(0, 0, 720, 180);
		panel1.setLayout(null);
		panel1.setBackground(Color.white);
		frame.add(panel1);
		
		tasks.setBounds(0,0,720,30);
		tasks.setFont(font);
		tasks.setBackground(fbBlue);
		tasks.setOpaque(true);
		
		ltask1.setBounds(20,60,150,30);
		ltask1.setBackground(da);
		ltask1.setOpaque(true);
		ltask1.setFont(font1);	
		
		ltask2.setBounds(190,60,150,30);
		ltask2.setBackground(da);
		ltask2.setOpaque(true);
		ltask2.setFont(font1);
		
		ltask3.setBounds(360,60,150,30);
		ltask3.setBackground(da);
		ltask3.setOpaque(true);
		ltask3.setFont(font1);
		
		ltask4.setBounds(530,60,150,30);
		ltask4.setBackground(da);
		ltask4.setOpaque(true);
		ltask4.setFont(font1);
		
		panel1.add(ltask1);
		panel1.add(ltask2);
		panel1.add(ltask3);
		panel1.add(ltask4);
		panel1.add(tasks);

		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getPanel1() {
		return panel1;
	}

	public void setPanel1(JPanel panel1) {
		this.panel1 = panel1;
	}

	public JButton getLtask1() {
		return ltask1;
	}

	public void setLtask1(JButton ltask1) {
		this.ltask1 = ltask1;
	}

	public JButton getLtask2() {
		return ltask2;
	}

	public void setLtask2(JButton ltask2) {
		this.ltask2 = ltask2;
	}

	public JButton getLtask3() {
		return ltask3;
	}

	public void setLtask3(JButton ltask3) {
		this.ltask3 = ltask3;
	}

	public JButton getLtask4() {
		return ltask4;
	}

	public void setLtask4(JButton ltask4) {
		this.ltask4 = ltask4;
	}

	public Dimension getDim() {
		return dim;
	}

	public void setDim(Dimension dim) {
		this.dim = dim;
	}

	public JLabel getTasks() {
		return tasks;
	}

	public void setTasks(JLabel tasks) {
		this.tasks = tasks;
	}

}
