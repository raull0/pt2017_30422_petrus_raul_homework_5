package presentation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import model.MonitoredData;
import r_w.Reader;
import r_w.Writer;

public class Controller {
	List<String> activities;
	List<MonitoredData> monitoredData;
	Writer writer2;
	Writer writer3;
	Writer writer4;
	Writer writer5;
	View view = new View();

	public Controller() {
		activities = Reader.readFile("C:/Users/ASUS/Downloads/workspace/HW5/Activities.txt");
		monitoredData = new ArrayList<MonitoredData>();

		getData();
		view.getLtask1().addActionListener(e -> task2());
		view.getLtask2().addActionListener(e -> task3());
		view.getLtask3().addActionListener(e -> task4());
		view.getLtask4().addActionListener(e -> task5());
		task1();

	}

	public void getData() {
		activities.stream().forEach(e -> {
			try {
				monitoredData.add(Reader.parsing(e));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

	}

	public void task1() {
		System.out.println(
				"------------------ The number of distinct days that appear in the monitoring data ------------------ \n                                                   "
						+ monitoredData.stream().map(e -> Reader.getDay(e.getStartTime())).distinct().count());

	}

	public Map<String, Integer> task2() {
		writer2 = new Writer("Task2");
		writer2.addRecords(
				"--------------------- Distinct action type : Number of occurrences in the log-----------------------\n");
		Map<String, Integer> map = monitoredData.stream()
				.collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.summingInt(e -> 1)));
		map.entrySet().stream().forEach(e -> writer2.addRecords(String.valueOf(e)));
		writer2.closeFile();

		return map;
	}

	public void task3() {
		writer3 = new Writer("Task3");
		writer3.addRecords(
				"-------------- For each day : Distinct action type : Number of occurrences in the log ---------------\n");
		Map<Integer, Map<String, Long>> map1 = monitoredData.stream()
				.collect(Collectors.groupingBy(e -> Reader.getDay(e.getStartTime()),
						Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));
		map1.entrySet().stream().forEach(e -> writer3.addRecords(String.valueOf(e)));
		writer3.closeFile();
	}

	public void task4() {
		writer4 = new Writer("Task4");
		writer4.addRecords(
				"--------------------------- For each activity : Duration > 10 hours ----------------------------------\n");
		Map<String, Integer> map2 = monitoredData.stream()
				.collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.summingInt(
						e -> (int) Reader.getDateDiff(e.getStartTime(), e.getEndTime(), TimeUnit.SECONDS))));
		map2.entrySet().stream().filter(e -> e.getValue() > 36000)
				.forEach(e -> writer4.addRecords(e.getKey() + " : " + (int) (e.getValue() / 3600)));
		writer4.closeFile();
	}

	public void task5() {
		writer5 = new Writer("Task5");
		writer5.addRecords(
				"---------------- 90% of the monitoring samples with duration less than 5 minutes-----------------------\n");
		Map<String, Integer> map = task2();
		Map<String, Long> lista1 = monitoredData.stream()
				.filter(e -> (int) Reader.getDateDiff(e.getStartTime(), e.getEndTime(), TimeUnit.SECONDS) < 300)
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

		List<String> list1 = lista1.entrySet().stream().map(e -> {
			if (map.containsKey(e.getKey())) {
				if (e.getValue() / map.get(e.getKey()) > 0.9) {
					return e.getKey();
				}
			}
			return "-";
		}).collect(Collectors.toList());
		list1.stream().forEach(e -> writer5.addRecords(e));
		writer5.closeFile();

	}

}
