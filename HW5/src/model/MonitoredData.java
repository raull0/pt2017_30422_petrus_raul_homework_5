package model;
import java.util.Date;

public class MonitoredData {
	public MonitoredData(Date startTime, Date endTime, String activityLabel) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	public MonitoredData() {
		// TODO Auto-generated constructor stub
	}
	private Date startTime;
	private Date endTime;
	private String activityLabel;

	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getActivityLabel() {
		return activityLabel;
	}
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	@Override
	public String toString() {
		return "MonitoredData [startTime=" + startTime + ", endTime=" + endTime + ", activityLabel=" + activityLabel
				+ "]";
	}
	
}
