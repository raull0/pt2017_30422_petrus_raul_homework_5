package r_w;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import model.MonitoredData;

public class Reader {

	public static List<String> readFile(String fileName) {
		List<String> activities = new ArrayList<String>();
		try {
			BufferedReader lineReader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = lineReader.readLine()) != null) {
				activities.add(line);
			}

			lineReader.close();
		} catch (IOException ex) {
			System.err.println(ex);
		}
		return activities;
	}
	
	public static MonitoredData parsing(String line) throws ParseException{
		MonitoredData result = new MonitoredData();
		
		String parse[] = line.split("		");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		result.setStartTime(format.parse(parse[0]));
		result.setEndTime(format.parse(parse[1]));
		result.setActivityLabel(parse[2]);
		
		return result;
	}
	
	public static int getDay(Date date){
		String s = String.valueOf(date);
		int day = Integer.parseInt(s.substring(8, 10));
		//System.out.println(day);
		return day;
	}
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	    
	}
}